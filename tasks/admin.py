from django.contrib import admin
from .models import Task, Module


admin.site.register(Task)
admin.site.register(Module)
