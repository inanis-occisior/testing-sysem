from django.contrib import admin
from .models import Group, UserNote

admin.site.register(Group)
admin.site.register(UserNote)